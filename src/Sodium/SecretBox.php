<?php
declare(strict_types=1);

namespace Silesiacoin\Phplib\Sodium;

class SecretBox implements SecretBoxInterface
{
    private $key;
    private $nonce;
    private $binKey;
    private $binNonce;
    private $paddedMessage;
    private $binMessage;
    private $binStrMessage;
    private $bytes;
    private $decrypted;

    /**
     * @return mixed
     */
    public function getBinStrMessage()
    {
        return $this->binStrMessage;
    }

    /**
     * @param mixed $binStrMessage
     */
    public function setBinStrMessage($binStrMessage): void
    {
        $this->binStrMessage = $binStrMessage;
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key): void
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getNonce()
    {
        return $this->nonce;
    }

    /**
     * @param mixed $nonce
     */
    public function setNonce($nonce): void
    {
        $this->nonce = $nonce;
    }

    /**
     * @return mixed
     */
    public function getDecrypted()
    {
        return $this->decrypted;
    }

    /**
     * @param mixed $decrypted
     */
    public function setDecrypted($decrypted): void
    {
        $this->decrypted = $decrypted;
    }

    /**
     * @return string
     */
    public function getBinKey(): string
    {
        return $this->binKey;
    }

    /**
     * @param string $binKey
     */
    public function setBinKey(string $binKey): void
    {
        $this->binKey = $binKey;
    }

    /**
     * @return string
     */
    public function getBinNonce() : string
    {
        return $this->binNonce;
    }

    /**
     * @param string|void $binNonce
     */
    public function setBinNonce($binNonce): void
    {
        $this->binNonce = $binNonce;
    }

    /**
     * @return mixed
     */
    public function getPaddedMessage()
    {
        return $this->paddedMessage;
    }

    /**
     * @param mixed $paddedMessage
     */
    public function setPaddedMessage($paddedMessage): void
    {
        $this->paddedMessage = $paddedMessage;
    }

    /**
     * @return mixed
     */
    public function getBinMessage()
    {
        return $this->binMessage;
    }

    /**
     * @param mixed $binMessage
     */
    public function setBinMessage($binMessage): void
    {
        $this->binMessage = $binMessage;
    }

    /**
     * @return int
     */
    public function getBytes(): int
    {
        return $this->bytes;
    }

    /**
     * @param int $bytes
     */
    public function setBytes(int $bytes): void
    {
        $this->bytes = $bytes;
    }

    public function __construct($key, $nonce, $bytes = 16)
    {
        $this->key = $key;
        $this->nonce = $nonce;
        $this->binKey = mhash_keygen_s2k(1, $this->key, $this->nonce, 32);
        $this->binNonce = mhash_keygen_s2k(1, $this->nonce, $this->key, 24);
        $this->bytes = $bytes;
    }

    /**
     * Provide secretbox $key and $noonce
     * @param $key
     * @param $nonce
     * @param int $bytes
     * @return mixed
     * @throws \Exception
     */
    public static function fromCredentials($key, $nonce, $bytes = 16)
    {
        $secretBox = new self($key, $nonce, $bytes);

        $secretBox->key = $key;
        $secretBox->nonce = $nonce;
        $secretBox->binKey = mhash_keygen_s2k(1, $secretBox->key, $secretBox->nonce, 32);
        $secretBox->binNonce = mhash_keygen_s2k(1, $secretBox->nonce, $secretBox->key, 24);
        $secretBox->bytes = $bytes;

        return $secretBox;
    }

    public function encodeString()
    {
        if (!$this->binNonce || !$this->binKey) {
            return false;
        }

        if (null === $this->bytes && null === $this->paddedMessage) {
            return false;
        }

        $this->paddedMessage = sodium_pad($this->paddedMessage, $this->bytes);
        $this->binMessage = sodium_crypto_secretbox($this->paddedMessage, $this->binNonce, $this->binKey);
        $this->binStrMessage = $this->binToStr($this->binMessage);
    }

    public function decodeString() : void
    {
        $this->decrypted = sodium_crypto_secretbox_open(
            $this->binMessage,
            $this->binNonce,
            $this->binKey
        );

        if ($this->decrypted) {
            $this->decrypted = sodium_unpad($this->decrypted, $this->bytes);
        }
    }

    public function binToStr($bin)
    {
        return base64_encode($bin);
    }

    public function decodeBinToStr($str)
    {
        return base64_decode($str);
    }
}
