<?php
declare(strict_types=1);

namespace Silesiacoin\Phplib\Sodium;

interface SecretBoxInterface
{
    public function __construct($key, $nonce, $bytes = 16);

    /**
     * Provide secretbox $key and $noonce
     * @param $key
     * @param $nonce
     * @param $bytes
     * @return mixed
     */
    public static function fromCredentials($key, $nonce, $bytes = 16);

    public function encodeString();

    public function decodeString();
}
